# `Python Package Index <https://en.wikipedia.org/wiki/Python_Package_Index>`_
# requirements for python3
#
# to install, use
# pip install -r {this_file_path}

--editable .

html5lib==1.1

lxml==4.6.2 # xml like document parsing
cssselect==1.1.0 # enable css selector feature in xml

pyparsing==2.4.7 # ciur grammar rule parser builder

python-dateutil==2.8.1 # parse datetime from different string formats

# http client lib to fetch documents by url
# http://stackoverflow.com/questions/29099404/
# ssl-insecureplatform-error-when-using-requests-package
requests[security]==2.25.1

pdfminer3k==1.3.4
